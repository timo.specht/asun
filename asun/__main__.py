from typing import List
from asun.automata import Asun
from asun.utilities import *


def postfix_to_asun(postfix: str, alphabet: List[str]) -> Asun:
    """
    Method uses alphabet und regex to construct an ASUN

    Parameters
    ----------
    postfix: str:
        regex in postfix notation that is converted into ASUN

    alphabet: List[str]:
        alphabet for the automaton

    Returns
    -------
    ASUN:
        created ASUN accepting words in the given regex
    """
    asun_stack = []
    for char in postfix:
        if is_letter(char):
            # if we read a letter we add a new ASUN to our stack
            asun_stack.append(Asun.from_letter(sigma=alphabet, letter=char))
        elif char in ['*', '!']:
            # in the cases '*' and '!' we only need one ASUN from the stack
            tmp1 = asun_stack.pop()
            if char == '*':
                asun_stack.append(tmp1.kleene_neg())
            else:
                asun_stack.append(-tmp1)
        else:
            # the operators '+', '|' and '&' need two automata from the stack
            tmp1 = asun_stack.pop()
            tmp2 = asun_stack.pop()
            if char == '+':
                asun_stack.append(tmp2 + tmp1)
            elif char == '|':
                asun_stack.append(tmp1 | tmp2)
            else:
                asun_stack.append(tmp1 & tmp2)

    return asun_stack[0]


def construct_alphabet(inp: str) -> List[str]:
    """
    Method creates an alphabet for the given input

    Parameters
    ----------
    inp: str:
        input word + regex for which alphabet is constructed

    Returns
    -------
    List[str]:
        the constructed alphabet

    Raises
    ------
    TypeError
        If given input is not a string
    """
    if not isinstance(inp, str):
        raise TypeError

    # for safety we always add \epsilon to the alphabet
    res = [CONST.EPSILON]

    for char in inp:
        if is_letter(char) and char not in res:
            res.append(char)

    return res


def regex_to_asun(inp_regex: str, inp_word: str) -> Asun:
    """
    Method uses input word und input regular expression to create ASUN. Therefore it first parses the input and
    checks if everything is alright and then begins the construction

    Parameters
    ----------
    inp_regex: str:
        input regular expression

    inp_word: str:
        input word


    Returns
    -------
    ASUN:
        constructed automaton
    """
    # delete all spaces since they are not necessary
    inp_regex = delete_spaces(inp_regex)
    inp_regex = to_lower(inp_regex)
    # if the regex contains \eps we need to check if \eps is really needed
    # for example we don't want input like '$$$$$$$$$$$$$$$$$' since this is equal to '$'
    # similarly if we have 'a$$$$$$$$$$$$b' the \eps are useless so we erase them
    inp_regex = remove_redundant_eps(inp_regex)
    # to make the input format easier we add the concat symbol afterwards
    inp_regex = add_concat(inp_regex)
    # transform infix ERE to postfix expression
    inp_regex = regex_to_postfix(inp_regex)
    # use ERE to build alphabet
    sigma = construct_alphabet(inp_regex+inp_word)
    # create ASUN
    return postfix_to_asun(inp_regex, sigma)


def check_membership(inp_regex: str, inp_word: str, debug: bool = False) -> bool:
    """
    Method checks if given word is in given regular expression

    Parameters
    ----------
    inp_regex: str:
        input regular expression

    inp_word: str:
        input word

    debug: bool:
        activate/deactivate debug mode


    Returns
    -------
    bool:
        True if word is accepted, false otherwise
    """
    # remove spaces
    inp_word = delete_spaces(inp_word)
    inp_word = to_lower(inp_word)
    # this is similar to the regex procedure
    inp_word = remove_redundant_eps(inp_word)
    automata = regex_to_asun(inp_regex, inp_word)
    return automata(inp_word, debug=debug)


def input_loop():
    """
    Method is just an input loop

    Parameters
    ----------
    Returns
    -------
    """
    print('\nTo exit the program just write quit or exit in one of the prompts\n\n'
          'Regex input has to be a fully bracketed expression\n'
          'Example: a & a* ---> (a & (a*))\n'
          '------------------------------------------------------------------------------------------\n\n')

    print('Synatx: & == and, | == or, ! == not, * == star, $ == epsilon\n'
          '-------------------------------------------------------------------------------------------\n\n')

    print('If you want to use the regex for multiple words, just leave the regex input empty\n'
          '-------------------------------------------------------------------------------------------\n\n')

    last_regex = ''
    while True:
        inp_regex = input('Please insert regex: ')

        if inp_regex == 'quit' or inp_regex == 'exit':
            print('Quitting...\n')
            break

        inp_word = input('Please insert word: ')

        if inp_word == 'quit' or inp_word == 'exit':
            print(f'Quitting...\n')
            break

        inp_regex = inp_regex if inp_regex != '' else last_regex

        try:
            print(f'\nResult of membership check: {check_membership(inp_regex, inp_word)} \n')
        except IndexError:
            print('\nMissing parentheses\n\n')
        except ValueError:
            print('\nSome Error occurred\n\n')

        last_regex = inp_regex


def main():
    input_loop()


if __name__ == '__main__':
    """
    The Method check_membership takes a regex and a word as input and checks if the word is in the regex.
    For easier use the input_loop method takes direct input.
    The special input symbols for a regex are:
        & == and, | == or, ! == not, * == star, $ == epsilon
    
    Always ensure that the regex is correctly bracketed!
    
    
    """
    main()

