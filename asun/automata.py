from __future__ import annotations

from collections import defaultdict
from functools import cached_property
from typing import Set, Tuple, Union
from uuid import uuid4
from asun.types import *


class Asun:
    def __init__(self):
        self.init_state: State = State(uuid4())
        self.fin_state: State = State(uuid4())
        self.states: List[State] = []
        self.sigma: Alphabet = []
        # \delta function for one state exists a list with the different letters of \sigma and where to go when letter is read
        self.trans_func: Transition_function = {}
        # \psi for universal or negation states
        self.sync_func: Dict[State, State] = {}
        # \phi function
        self.special_func: Dict[State, Union[str, State]] = {}
        # contains all edges of the graph saved as mapping between the position and the positions it is connected to
        self.com_graph: Edges = {}
        # Nodes are all positions
        self.com_graph_verts: Vertices = []
        # saves the covering positions for a special position. Used in membership check procedure
        self.covering_set: Dict[Position, Set[int]] = defaultdict(set)

    @classmethod
    def from_letter(cls, sigma: Alphabet, letter: str = CONST.EPSILON, empty: bool = False):
        # uses the construction in Fig. 2 to construct an ASUN for one letter or the empty set
        res = cls()
        res.states = [State(uuid4()) for _ in range(2)]
        res.init_state = res.states[0]
        res.fin_state = res.states[-1]
        res.sigma = sigma
        res.trans_func = {s: {c: [] for c in sigma} for s in res.states}

        if not empty:
            # if we want to construct ASUN for one letter we just need to connect init and fin with the letter
            res.trans_func[res.init_state][letter].append(res.fin_state)

        # build \phi for the ASUN
        res.construct_special_func(None, None, regex_type=CONST.LETTER)

        return res

    @property
    def get_n(self) -> int:
        # get the highest idx of the Positions
        return int(len(self.com_graph_verts) / len(self.states))

    @property
    def special_positions(self) -> List[Position]:
        # return all positions that contain a special state
        return [p for p in self.com_graph_verts if p.special]

    @cached_property
    def special_states(self) -> List[State]:
        # return all negation and universal states
        return [s for s in self.states if s.typ in [CONST.UNIVERSAL, CONST.NEGATION]]

    def __or__(self, asun: Asun) -> Asun:
        return self.construct_and_or(asun, universal=False)

    def __and__(self, asun: Asun) -> Asun:
        return self.construct_and_or(asun)

    def __neg__(self) -> Asun:
        return self.kleene_neg(kleene=False)

    def __add__(self, asun: Asun) -> Asun:
        # construction for concat
        res = Asun()
        res.states = self.states + asun.states
        res.init_state = self.init_state
        res.fin_state = asun.fin_state
        res.sigma = list(set(self.sigma) | set(asun.sigma))

        self.sync_func.update(asun.sync_func)
        res.sync_func = self.sync_func

        for s in res.states:
            # decide which function to take for which state
            res.trans_func[s] = self.trans_func[s] if s in self.states else asun.trans_func[s]

        # connect the two ASUNs by an \epsilon connection
        res.trans_func[self.fin_state][CONST.EPSILON].append(asun.init_state)

        # construct \phi
        res.construct_special_func(self, asun, regex_type=CONST.CONCAT)

        return res

    def __call__(self, word: str, debug: bool = False) -> bool:
        # check if the input is correct
        for char in word:
            if char not in self.sigma:
                return False

        if debug:
            print(f'Initial State: {self.init_state}\n')

            print(f'Final State: {self.fin_state}\n')

            print('Transition Function:')
            for state in self.states:
                print(f'{state} ---> {self.trans_func[state]}')
            print()
            self.construct_computation_graph(word, debug=debug)
            return self.membership_check(word, debug=debug)

        self.construct_computation_graph(word)

        return self.membership_check(word)

    def construct_and_or(self, asun: Asun, universal: bool = True) -> Asun:
        # 'or' and 'and' work similar
        res = Asun()

        if universal:
            # difference to 'or' the init state has to be universal
            res.states = [State(uuid4(), CONST.UNIVERSAL)] + self.states + asun.states + [State(uuid4())]
        else:
            res.states = [State(uuid4())] + self.states + asun.states + [State(uuid4())]

        res.init_state = res.states[0]
        res.fin_state = res.states[-1]
        res.sigma = list(set(self.sigma) | set(asun.sigma))

        self.sync_func.update(asun.sync_func)
        res.sync_func = self.sync_func

        for state in res.states:
            if state in self.states:
                res.trans_func[state] = self.trans_func[state]
            elif state in asun.states:
                res.trans_func[state] = asun.trans_func[state]
            else:
                # because we have new states we need to initialize their transition function
                res.trans_func[state] = {c: [] for c in res.sigma}

        # \epsilon split at the init state
        res.trans_func[res.init_state][CONST.EPSILON].extend([self.init_state, asun.init_state])

        for state in [self.fin_state, asun.fin_state]:
            # \epsilon join at fin state
            res.trans_func[state][CONST.EPSILON].extend([res.fin_state])

        if universal:
            # in the 'and' case we additionally have to sync the fin state with the init state so that both ways arrive at the 'same' time
            res.sync_func[res.init_state] = res.fin_state

        # construct \phi this is also different in the 'and' case since we have special states
        res.construct_special_func(self, asun, regex_type=CONST.AND if universal else CONST.OR)

        return res

    def kleene_neg(self, kleene: bool = True) -> Asun:
        # like 'and' and 'or' '*' and 'negation' work also pretty similar
        res = Asun()
        if not kleene:
            # in the negation case weh have to label the init state
            res.states = [State(uuid4(), CONST.NEGATION)] + self.states + [State(uuid4())]
        else:
            res.states = [State(uuid4())] + self.states + [State(uuid4())]
        res.init_state = res.states[0]
        res.fin_state = res.states[-1]
        # the alphabet stays the same
        res.sigma = self.sigma

        res.sync_func = self.sync_func

        for state in res.states:
            # either take the existing function or initialize new function for init and fin
            res.trans_func[state] = self.trans_func[state] if state in self.states else {c: [] for c in self.sigma}

        # in both cases we have to insert the old ASUN between the new init and fin state
        res.trans_func[res.init_state][CONST.EPSILON].append(self.init_state)
        res.trans_func[self.fin_state][CONST.EPSILON].append(res.fin_state)

        if kleene:
            # if we have '*' we need to build two loops one from new init to fin, since kleene contains \epsilon and a loop in the old ASUN
            res.trans_func[res.init_state][CONST.EPSILON].append(res.fin_state)
            res.trans_func[self.fin_state][CONST.EPSILON].append(self.init_state)

        if not kleene:
            # in the negation case we only have to build one epsilon connection from init to fin
            res.sync_func[res.init_state] = res.fin_state

        # Again build \phi which differs in both cases
        res.construct_special_func(self, Asun(), regex_type=CONST.KLEENE if kleene else CONST.NEG)

        return res

    def construct_special_func(self, asun_1: Union[Asun, None], asun_2: Union[Asun, None], regex_type: str):
        # in both cases \phi of the init/final state are the empty symbol
        self.special_func[self.init_state] = CONST.EMPTY
        self.special_func[self.fin_state] = CONST.EMPTY

        if regex_type == CONST.LETTER:
            # if we just have one letter we are finished since we only have two states (init and fin)
            return

        # \phi function as described in the paper
        if regex_type in [CONST.KLEENE, CONST.OR, CONST.CONCAT]:
            for state in self.states[1: -1]:
                # q \in ASUN_1 --> \phi(q) = \phi_1(q)
                if state in asun_1.states:
                    self.special_func[state] = asun_1.special_func[state]

                # q \in ASUN_2 --> \phi(q) = \phi_2(q)
                elif state in asun_2.states:
                    self.special_func[state] = asun_2.special_func[state]
        else:
            # exclude q_{in} and q_{fin}
            for state in self.states[1: -1]:
                # q \in ASUN_1 and \phi_1(q) != ⊥ --> \phi(q) = \phi_1(q)
                if state in asun_1.states and asun_1.special_func[state] != CONST.EMPTY:
                    self.special_func[state] = asun_1.special_func[state]

                # q \in ASUN_1 and \phi_1(q) == ⊥ --> \phi(q) = q_{in}
                elif state in asun_1.states and asun_1.special_func[state] == CONST.EMPTY:
                    self.special_func[state] = self.init_state

                # q \in ASUN_2 and \phi_2(q) != ⊥ --> \phi(q) = \phi_2(q)
                elif state in asun_2.states and asun_2.special_func[state] != CONST.EMPTY:
                    self.special_func[state] = asun_2.special_func[state]

                # q \in ASUN_2 and \phi_2(q) == ⊥ --> \phi(q) = q_{in}
                elif state in asun_2.states and asun_2.special_func[state] == CONST.EMPTY:
                    self.special_func[state] = self.init_state

    def construct_computation_graph(self, word: str, debug: bool = False):

        # if the word equal \eps we don't want a higher idx than 0 since we don't actually read an input
        len_word = 1 if word == CONST.EPSILON else (len(word) + 1)

        # first we construct all nodes (positions) ...
        v = [Position(s, i, s.typ in [CONST.UNIVERSAL, CONST.NEGATION]) for s in self.states for i in range(len_word)]
        self.com_graph_verts = v

        # then all edges
        self.com_graph = {p_1: [p_2 for p_2 in v if self.is_succ(p_1, p_2, CONST.EPSILON + word)] for p_1 in v}

        if debug:
            for vert, edges in self.com_graph.items():
                print(f'Position: {vert} has #Edges: {len(edges)}')

    def is_succ(self, p_1: Position, p_2: Position, word: str) -> bool:
        if p_1.state == p_2.state:
            return False

        # a positions is a successor of an position if it is an \epsilon successor ...
        if p_1.index == p_2.index and p_2.state in self.trans_func[p_1.state][CONST.EPSILON]:
            return True

        # or \sigma_{i+1} successor
        if p_2.index == p_1.index + 1 and p_2.state in self.trans_func[p_1.state][word[p_1.index + 1]]:
            return True

        return False

    def full_order(self) -> List[List[Position]]:
        all_pos = self.special_positions
        full_order = []
        while all_pos:
            # since special positions don't have to be connected we could potentially build multiple orders
            res, all_pos = self.order(all_pos, full_order)

            full_order = self.insert_order(full_order, res)

        return full_order

    def order(self, positions: List[Position], full_order: List[List[Position]]) -> Tuple[
        List[Position], List[Position]]:
        order = [positions[0]]
        queue = [positions.pop(0)]
        # we want to build an order by beginning with the innermost state and end with the outermost
        while queue:
            p_1 = queue.pop()
            # all positions with same state and different index have same ordering
            tmp = [p for p in positions if p.state == p_1.state and p_1.index != p.index]
            order.extend(tmp)
            # all positions added to the order need to be deleted
            positions = [p for p in positions if p not in order]

            # look if there is another special state surrounding this one
            # additionally verify that we didn't process this one already
            if self.special_func[p_1.state] != CONST.EMPTY and self.not_in_order(self.special_func[p_1.state],
                                                                                 full_order):
                next_special_state = self.special_func[p_1.state]
                # we need to find the correct position for the given state
                next_special_pos = self.get_position(next_special_state, positions)
                queue.append(next_special_pos)
                order.append(next_special_pos)

        return order, positions

    def insert_order(self, full_order: List[List[Position]], to_insert: List[Position]) -> List[List[Position]]:
        extend_flag = False

        # traverse all existing orders
        for order_idx, order in enumerate(full_order):
            # just take the Position with idx 0 for every state
            for idx in range(0, len(order), self.get_n):
                if self.special_func[to_insert[0].state] != CONST.EMPTY:
                    if order[idx].state == self.special_func[to_insert[0].state]:
                        # insert the order before the surrounding one
                        order = order[0:idx] + to_insert + order[idx:]
                        # change the order at the idx in full_order
                        full_order[order_idx] = order
                        extend_flag = True

        if not extend_flag:
            # can't add given order to an existing one --> add it as new separate order
            full_order.append(to_insert)

        return full_order

    def membership_check(self, word: str, debug: bool = False) -> bool:
        # if we have no special states we don't have to build an order
        if not self.special_states:
            return self.do_membership_check(word, [])

        assert len(self.special_states) >= 1

        full_order = self.full_order()

        if debug:
            print()
            for state in self.states:
                if state.typ != CONST.EXISTENTIAL:
                    print(f'phi({state}) ---> {self.special_func[state]}')

            print()
            for q1, q2 in self.sync_func.items():
                print(f'psi({q1} ---> {q2}')

            print(f'\nFull order: {full_order}\n')

        for order in full_order:
            # if we have special states that are not linked we get multiple orders and we check every order
            if self.do_membership_check(word, order):
                return True

        return False

    def do_membership_check(self, word: str, order: List[Position]) -> bool:
        # save the results of the covering of the right and left child for one position needed to construct the final set
        sets: Dict[Tuple[Position, int], Set[int]] = dict()

        for p in self.com_graph_verts:
            p.visited = False
            p.search_idx = -1

        for p in order:
            # create the covering sets for every position in our order beginning with the leftmost
            self.covering_set[p] = self.find_sync(p, word, sets)

        # after finding covering positions we can traverse our graph for accepting paths
        return self.find_path(self.com_graph_verts[0])

    def find_sync(self, q: Position, word: str, sets: Dict[Tuple[Position, int], Set[int]]) -> Set[int]:
        if q.state.typ == CONST.UNIVERSAL:
            # if we have an universal state we get two \epsilon successors (right and left)
            sets[(q, CONST.LEFT)] = set()
            sets[(q, CONST.RIGHT)] = set()
            # with the update on both children we are traversing the graph
            self.update(self.com_graph[q][CONST.LEFT], q, CONST.LEFT, sets)
            self.update(self.com_graph[q][CONST.RIGHT], q, CONST.RIGHT, sets)
            # we want to find covering positions that are reachable from both sides, since we have an '&'
            return sets[(q, CONST.LEFT)].intersection(sets[(q, CONST.RIGHT)])
        else:
            # if the state isn't a universal state, it's a negation state and has only successors, which is denoted left
            sets[(q, CONST.LEFT)] = set()
            # if some state has only one successor we always assume that it's the left one
            self.update(self.com_graph[q][CONST.LEFT], q, CONST.LEFT, sets)
            # if word = \eps we need to set the highest index to 0 by setting the len of the word to 1
            len_word = 1 if word == CONST.EPSILON else (len(word) + 1)
            # we achieve the negation by taking a difference between the found covering positions and the available indices
            return {i for i in range(q.index, len_word)}.difference(sets[(q, CONST.LEFT)])

    def update(self, s: Position, q: Position, d: int, sets: Dict[Tuple[Position, int], Set[int]]):
        # either we are on the same level or our position is a leaf in that case we can return
        if s.search_idx == q.index or self.is_leaf(s):
            return
        s.search_idx = q.index
        if s.state.typ == CONST.EXISTENTIAL:
            if len(self.trans_func[s.state][CONST.EPSILON]) == 2:
                # if we have a branch we need to  update both children
                self.update(self.com_graph[s][CONST.LEFT], q, d, sets)
                self.update(self.com_graph[s][CONST.RIGHT], q, d, sets)
            elif self.com_graph[s][CONST.LEFT] == Position(self.sync_func[q.state], s.index):
                # if we reached a covering position we can update our set
                sets[(q, d)].update({s.index})
            else:
                # if we didn't reach a covering position and we got only one child we need only one update (assume it's the left one)
                self.update(self.com_graph[s][CONST.LEFT], q, d, sets)
        else:
            # of we get to this point s is a special position ...
            for j in self.covering_set[s]:
                # so we can continue searching for positions from their sets
                self.update(self.get_position(self.sync_func[s.state], self.com_graph_verts, j), q, d, sets)
        return

    def find_path(self, q: Position) -> bool:
        # if we read the final letter and are in the final state we can accept the word
        if self.is_accepting(q):
            return True
        # if we already visited the note or it's a leaf we can't go further and so we can't accept
        if q.visited or self.is_leaf(q):
            return False
        # we need to keep track which nodes we already visited
        q.visited = True
        if q.state.typ == CONST.EXISTENTIAL:
            if len(self.trans_func[q.state][CONST.EPSILON]) == 2:
                # check if one of the ways lead to the accepting state
                return self.find_path(self.com_graph[q][CONST.RIGHT]) or self.find_path(self.com_graph[q][CONST.LEFT])
            # else we got only one child and need to check it
            return self.find_path(self.com_graph[q][CONST.LEFT])
        else:
            # if we got a special position ...
            for j in self.covering_set[q]:
                # we need to check if beginning by their covering position we can go to the accepting state
                if self.find_path(self.get_position(self.sync_func[q.state], self.com_graph_verts, j)):
                    return True
            return False

    @staticmethod
    def get_position(q: State, positions: List[Position], idx: int = 0) -> Position:
        # search for Position in our  vertices. Assumption is that the Position exists
        for p in positions:
            if p.state == q and p.index == idx:
                return p

        raise ValueError

    @staticmethod
    def not_in_order(state: State, full_order: List[List[Position]]) -> bool:
        for order in full_order:
            for p in order:
                if p.state == state:
                    return False

        return True

    def is_leaf(self, vert: Position) -> bool:
        # does the vertex has outgoing edges
        return not self.com_graph[vert]

    def is_accepting(self, vert: Position) -> bool:
        # first check if we have an accepting state and then check if the index == n
        return vert.state == self.fin_state and vert.index == self.get_n - 1

    def __str__(self) -> str:
        res = f"States = {self.states} \n" \
              f"Alphabet = {list(self.sigma)} \n" \
              f"Initial State = {self.init_state} \n" \
              f"Final States = {self.fin_state} \n" \
              f"Transitions Function = {self.trans_func}\n" \
              f"Sync Function = {self.sync_func}\n" \
              f"Special Function = {self.special_func}\n"
        return res

    def get_dict_rep(self) -> Dict[str, Union[List[State], State, Alphabet, Transition_function]]:
        res = dict()
        res['states'] = self.states
        res['init_state'] = self.init_state
        res['fin_state'] = self.fin_state
        res['Alphabet'] = self.sigma
        res['trans_func'] = self.trans_func
        return res
